import { AuthGuard } from './auth/auth.guard';
import { Page3Component } from './components/page3/page3.component';
import { Page2Component } from './components/page2/page2.component';
import { Page1Component } from './components/page1/page1.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [AuthGuard], children: [

    { path: '' , redirectTo: 'pagina1', pathMatch: 'full'},
    { path: 'pagina1' , component: Page1Component, canActivate: [AuthGuard]},
    { path: 'pagina2' , component: Page2Component, canActivate: [AuthGuard]},
    { path: 'pagina3' , component: Page3Component, canActivate: [AuthGuard]},

  ]},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
